  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Setting</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-6 col-xs-6">

       

          <div class="box">

            <div class="box-header">
            <h4> Setting Menu Data</h4>
              <!-- <a href="<?= base_url() ?>keloladata/tambah"><button type="button" class="btn btn-sm btn-primary">Tambah Data</button></a>  -->
              <!--<a onclick="return confirm('Anda Ingin Non Aktifkan Semua User ?')" href="<?= base_url() ?>kelolauser/close_masal"><button type="button" class="btn btn-sm btn-danger">Non Aktifkan Massal</button></a> -->
              <!--<a onclick="return confirm('Anda Ingin Aktifkan Semua User ?')" href="<?= base_url() ?>kelolauser/open_masal"><button type="button" class="btn btn-sm btn-success">Aktifkan Massal</button></a> -->
            </div>

            <!-- /.box-header -->
            <div class="box-body">
              <table style="width:100%" id="" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th width="50px">Nomor</th>
                    <th>Pengaturan</th>
                    <th>Nilai</th>
                    <th>Edit</th>

                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 0;
                  foreach ($alldata as $all) :
                    $no++;
                  ?>
                   <?php echo form_open_multipart('setting/aksi_edit');  ?>
                    <tr>
                    <td><?= $no ?></td>
                    <td><?= $all->nama ?> </td>
                    <td>
                    <input type="hidden" name="id" value="<?=$all->id?>">
                    <input type="text" name="nilai" value="<?=$all->nilai?>">
                    </td>
                    <td><button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-pencil" style="font-size:12px"></i></button></td>
                    </tr>
                    <?php echo form_close(); ?>
                  <?php endforeach; ?>

                </tbody>


              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->



        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

  <?php include(__DIR__ . "/../template/footer.php"); ?>




  <script>
    $('#example').DataTable({
      // dom: 'Bfrtip',
      // buttons: [
      //   //'copyHtml5',
      //   'excelHtml5',
      //   //'csvHtml5',
      //   //'pdfHtml5'
      // ],
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "scrollX": true,
      "fixedHeader": true
    });
  </script>