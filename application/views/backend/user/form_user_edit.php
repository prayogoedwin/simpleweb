  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Tambah User</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Tambah Admin</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      

    <div class="col-md-8">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Horizontal Form</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open_multipart('kelolauser/aksi_edit');  ?>
              <div class="box-body">
              




                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
                  <div class="col-sm-10">
                    <input type="hidden" name="id" class="form-control" id="inputEmail3" value="<?=$detail->id_pengguna?>" required="required">
                    <input type="hidden" name="role" class="form-control" id="inputEmail3" value="<?=$detail->role?>" required="required">
                    <input type="text" name="nama" class="form-control" id="inputEmail3"  value="<?=$detail->nama?>" required="required">
                                    
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
                  <div class="col-sm-10">
                    <input type="text" required="required" name="username"  value="<?=$detail->username?>" class="form-control" id="inputEmail3" placeholder="Usermame">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-10">
                    <input type="password"  name="password" class="form-control" id="inputPassword3" placeholder="Kosongi Jika tidak ingin merubah password">
                  </div>
                </div>
                

               
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Edit</button>
              </div>
              <!-- /.box-footer -->
              <?php echo form_close(); ?>
          </div>
        </div>

    
      

    </section>
    <!-- /.content -->
  </div>

  <?php include(__DIR__ . "/../template/footer.php"); ?>