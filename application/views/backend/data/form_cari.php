  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Edit Data</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Edit Data</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      

    <div class="col-md-8">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Horizontal Form</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open_multipart('keloladata/search_v2');  ?>
              <div class="box-body">
              
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Umur</label>
                  <div class="col-sm-10">
                  
                  <select style="width:100%" class="js-example-basic-multiple" name="umur[]" multiple="multiple">
                      <?php foreach($alldata_umur as $umur): ?>
                      <option selected style="width:100%" value="<?=$umur->umur?>"><?=$umur->umur?></option>
                      <?php endforeach; ?>
                  </select>
                                    
                  </div>
                </div>

                <br/>
                <br/>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Alamat</label>
                  <div class="col-sm-10">
                  
                  <select style="width:100%" class="js-example-basic-multiple" name="alamat[]" multiple="multiple">
                      <?php foreach($alldata_alamat as $alamat): ?>
                      <option selected style="width:100%" value="<?=$alamat->alamat?>"><?=$alamat->alamat?></option>
                      <?php endforeach; ?>
                  </select>
                                    
                  </div>
                </div>
                
                

               
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Cari</button>
              </div>
              <!-- /.box-footer -->
              <?php echo form_close(); ?>
          </div>
        </div>

    
      

    </section>
    <!-- /.content -->
  </div>

  <?php include(__DIR__ . "/../template/footer.php"); ?>

  <script>
      $(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
  </script>