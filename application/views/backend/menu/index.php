  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Kelola Menu</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-12 col-xs-12">

          <div class="box">
            <div class="box-header">
              <a href="<?= base_url() ?>kelolamenu/tambah"><button type="button" class="btn btn-sm btn-primary">Tambah Data</button></a>
              <!--<a onclick="return confirm('Anda Ingin Non Aktifkan Semua User ?')" href="<?= base_url() ?>kelolauser/close_masal"><button type="button" class="btn btn-sm btn-danger">Non Aktifkan Massal</button></a> -->
              <!--<a onclick="return confirm('Anda Ingin Aktifkan Semua User ?')" href="<?= base_url() ?>kelolauser/open_masal"><button type="button" class="btn btn-sm btn-success">Aktifkan Massal</button></a> -->
            </div>

            <!-- /.box-header -->
            <div class="box-body">
              <table style="width:100%" id="example" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th width="50px">Nomor</th>
                    <th>Nama</th>
                    <!-- <th>URL</th> -->
                    <th>Icon</th>
                    <th width="100px">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 0;
                  foreach ($alldata as $all) :
                    $no++;
                  ?>

                    <td><?= $no ?></td>
                    <td><?= $all->nama ?>  <?php if($all->tipe == 1){ ?> (Menu Utama) <?php }?></td>
                    <!-- <td><?= $all->url ?></td> -->
                    <td><i class="fa <?=$all->icon?>">&nbsp;&nbsp;&nbsp;<?= $all->icon ?></i> </td>
                    <td>
                      
                      <?php if($all->tipe == 1){ ?>
                        <a href="#" onclick="return alert('Menu Utama Tidak Bisa Diedit!')"><button type="button" class="btn btn-sm btn-warning"><i class="fa fa-pencil" style="font-size:12px"></i></button></a>
                        <a href="#"><button onclick="return alert('Menu Utama Tidak Bisa Dihapus!')" type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash" style="font-size:12px"></i></button></a>
                      <?php }else{ ?>
                        <a href="<?= base_url() ?>kelolamenu/edit/<?= $all->id ?>"><button type="button" class="btn btn-sm btn-warning"><i class="fa fa-pencil" style="font-size:12px"></i></button></a>
                       <a href="<?= base_url() ?>kelolamenu/hapus/<?= $all->id ?>"><button onclick="return confirm('Anda ingin menghapus ?')" type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash" style="font-size:12px"></i></button></a>
                      <?php } ?>
                    </td>

                    </tr>
                  <?php endforeach; ?>

                </tbody>

               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->



        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

  <?php include(__DIR__ . "/../template/footer.php"); ?>

  <!-- <script>
    $(document).ready(function() {

      var table = $('#example').DataTable({
        dom: 'Bfrtip',
        buttons: [
          //'copyHtml5',
          'excelHtml5',
          //'csvHtml5',
          //'pdfHtml5'
        ],
        initComplete: function() {
          count = 0;
          this.api().columns().every(function() {
            var title = this.header();
            //replace spaces with dashes
            title = $(title).html().replace(/[\W]/g, '-');
            var column = this;
            var select = $('<select style="width:100%" id="' + title + '" class="select2" ></select>')
              .appendTo($(column.header()).empty())
              .on('change', function() {
                //Get the "text" property from each selected data 
                //regex escape the value and store in array
                var data = $.map($(this).select2('data'), function(value, key) {
                  return value.text ? '^' + $.fn.dataTable.util.escapeRegex(value.text) + '$' : null;
                });

                //if no data selected use ""
                if (data.length === 0) {
                  data = [""];
                }

                //join array into string with regex or (|)
                var val = data.join('|');

                //search for the option(s) selected
                column
                  .search(val ? val : '', true, false)
                  .draw();
              });

            column.data().unique().sort().each(function(d, j) {
              select.append('<option value="' + d + '">' + d + '</option>');
            });

            //use column title as selector and placeholder
            $('#' + title).select2({
              multiple: true,
              closeOnSelect: false,
              placeholder: title
            });

            //initially clear select otherwise first option is selected
            $('.select2').val(null).trigger('change');
          });
        }
      });
    });
  </script> -->


  <script>
    
    $('#example').DataTable( {
        // dom: 'Bfrtip',
        // buttons: [
        //     //'copyHtml5',
        //     'excelHtml5',
        //     //'csvHtml5',
        //     //'pdfHtml5'
        // ],
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "scrollX": true,
      "fixedHeader": true
    });

</script>