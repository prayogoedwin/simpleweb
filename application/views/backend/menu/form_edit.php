  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Edit Menu</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Edit Menu</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      

    <div class="col-md-8">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Horizontal Form</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open_multipart('kelolamenu/aksi_edit');  ?>
              <div class="box-body">
              
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
                  <div class="col-sm-10">
                  <input type="hidden" name="id" value="<?=$detail->id?>" class="form-control" id="inputEmail3" placeholder="Nama" required="required">
                    <input type="text" name="nama" value="<?=$detail->nama?>" class="form-control" id="inputEmail3" placeholder="Nama" required="required">
                                    
                  </div>
                </div>
                <!-- <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">URL</label>
                  <div class="col-sm-10">
                  <input type="text" name="url" value="<?=$detail->url?>" class="form-control" id="inputEmail3" placeholder="URL" required="required">
                  </div>
                </div> -->
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Icon <i class="fa <?=$detail->icon?>"></i></label>
                  <div class="col-sm-10">
                    <input type="text" required="required" value="<?=$detail->icon?>" name="icon" class="form-control" id="inputPassword3" placeholder="Icon">
                    Untuk melihat list icon klik <a target="BLANK" href="https://adminlte.io/themes/AdminLTE/pages/UI/icons.html">disini (fontawesome)</a> 
                  </div>
                </div>
                

               
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Edit</button>
              </div>
              <!-- /.box-footer -->
              <?php echo form_close(); ?>
          </div>
        </div>

    
      

    </section>
    <!-- /.content -->
  </div>

  <?php include(__DIR__ . "/../template/footer.php"); ?>