  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Tambah Isi Data</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Tambah Data</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      

    <div class="col-md-8">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Horizontal Form</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open_multipart('kelolaisi/aksi_tambah');  ?>
              <div class="box-body">

              

              <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">ID</label>
                  <div class="col-sm-10">
                  <select class="form-control" name="nipcari" id="nipcari"></select>
                  </div>
                </div>

                <br/>
                <br/>

                <div class="form-group" hidden>
                  <label for="inputEmail3" class="col-sm-2 control-label">ID</label>
                  <div class="col-sm-10">
                  
                    <input type="hidden" id="id" required name="id" class="form-control" id="inputEmail3" placeholder="ID" required="required">
                                    
                  </div>
                </div>
              
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
                  <div class="col-sm-10">
                    <input type="text" id="nama" readonly name="nama" class="form-control" id="inputEmail3" placeholder="Nama" required="required">
                                    
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Alamat</label>
                  <div class="col-sm-10">
                    <textarea name="alamat" readonly id="alamat"class="form-control" ></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Umur</label>
                  <div class="col-sm-10">
                    <input type="text"  id="umur" readonly required="required" name="umur" class="form-control" id="inputPassword3" placeholder="Umur">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Keterangan</label>
                  <div class="col-sm-10">
                  <textarea name="ket"  id="ket"class="form-control" ></textarea>
                    <!-- <input type="text"  id="ket" required="required" name="ket" class="form-control" id="inputPassword3" placeholder="Ket"> -->
                  </div>
                </div>
                

               
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Tambah</button>
              </div>
              <!-- /.box-footer -->
              <?php echo form_close(); ?>
          </div>
        </div>

    
      

    </section>
    <!-- /.content -->
  </div>

  <?php include(__DIR__ . "/../template/footer.php"); ?>

  <script>
    $(function () {
        $('#nipcari').select2({
            ajax: {
                url: "<?=base_url()?>kelolaisi/ajax_cari_id/",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    var query = {
                        search: params.term
                    }

                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                },
                processResults: function (data) {
                    return {
                        results: data.results
                    };
                },
                cache: true
            },
            placeholder: 'Cari Data',
            minimumInputLength: 1,
            escapeMarkup: function (markup) { return markup; },
            templateResult: formatRepo,
            templateSelection: formatRepoSelection,
            language: {
                noResults: function (params) {
                    return "ID Tidak Ditemukan.";
                }
            }
        });

        function formatRepo (repo) {
            if (repo.loading) return repo.nama;
            var markup = '<li value='+repo.nip+'>'+repo.nama+' | '+repo.id+'</li>';
            // var markup = '<li value='+repo.nip+'>'+repo.nip+' '+repo.nama+'</li>';
            return markup;
        }

        function formatRepoSelection (repo) {
            return repo.nip;
        }

        $('#nipcari').change(function(){
           
            var id = $('#nipcari').select2('data')[0].id;
            var nama = $('#nipcari').select2('data')[0].nama;
            var alamat = $('#nipcari').select2('data')[0].alamat;
            var umur = $('#nipcari').select2('data')[0].umur;
           
            
            
            // $('#nip').val(nip);
           
            $('#id').val(id);
            $('#nama').val(nama);
            $('#alamat').val(alamat);
            $('#umur').val(umur);

        });

        $('#btnReset').click(function(){
            $('#nipcari').val(" ");
            $('#id').val(" ");
            $('#nama').val(" ");
            $('#alamat').val(" ");
            $('#umur').val(" ");
            
        });
    });
</script>