<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?= base_url('assets/user.png') ?>" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?= get_user($this->session->userdata('id_pengguna'))->nama; ?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>

    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MENU KONTEN</li>
      <li><a href="<?= base_url() ?>kelolamenu"><i class="fa fa-bars"></i> <span>Manajemen Menu</span></a></li>
      <li><a href="<?= base_url() ?>kelolauser"><i class="fa fa-users"></i> <span>Users</span></a></li>
      <li><a href="<?= base_url() ?>keloladata"><i class="fa fa-database"></i> <span>Masterdata</span></a></li>
      <li><a href="<?= base_url() ?>keloladata/form"><i class="fa fa-edit"></i> <span>Masterdata Form</span></a></li>
      <li><a href="<?= base_url() ?>kelolaisi"><i class="fa fa-keyboard-o"></i> <span>Isi Data</span></a></li>
      <li><a href="<?= base_url() ?>kelolahistori"><i class="fa fa-history"></i> <span>Histori Log</span></a></li>
      <li><a href="<?= base_url() ?>setting"><i class="fa fa-gear"></i> <span>Setting</span></a></li>
      <?php
      $menus = $this->db->query("SELECT * FROM si_menu WHERE tipe = 0")->result();
      foreach ($menus as $mn) : ?>
        <li><a href="<?= base_url('menu/index/'.$mn->id) ?>"><i class="fa <?=$mn->icon?>"></i> <span><?=ucwords($mn->nama)?></span></a></li>
      <?php endforeach; ?>
    </ul>
  </section>
</aside>