<body class="hold-transition skin-red  sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?=base_url('dashboard')?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-lg"><b>Simple</b> Web</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-mini"><b>S</b>W</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">      
          
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?=base_url('assets/user.png')?>" class="user-image" alt="User Image">
              <span class="hidden-xs"> <?= get_user($this->session->userdata('id_pengguna'))->nama; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?=base_url('assets/user.png')?>" class="img-circle" alt="User Image">

                <p>
                 <?= get_user($this->session->userdata('id_pengguna'))->nama; ?>
                  <!-- <small>Online</small> -->
                </p>
              </li>
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?=base_url('dashboard/edit_password')?>" class="btn btn-default btn-flat">Ganti Password</a>
                </div>
                
                <div class="pull-right">
                  <a href="<?=base_url('portal/logout')?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->