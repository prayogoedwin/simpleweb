
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Login</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Themesbrand" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App Icons -->
        <link rel="shortcut icon" href="<?=base_url()?>assets/login/favicon.ico">

        <!-- App css -->
        <link href="<?=base_url()?>assets/login/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/login/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/login/style.css" rel="stylesheet" type="text/css" />

    </head>


    <body>
        <!-- Loader -->
        <div id="preloader"><div id="status"><div class="spinner"></div></div></div>

        <!-- Begin page -->
        <div class="accountbg"  style="background-image: url(<?=base_url()?>assets/login/web.jpeg); background-size: 100%; width: 100%"></div>
        <div class="wrapper-page">

            <div class="card" style="background-color: #00000040; border-radius: 5px">
                <div class="card-body">

                    <div class="p-3">
                        <div class="float-right text-right">
                            <h4 class="font-18 mt-3 m-b-5" style="color: white">Portal Admin </h4>
                            <!--<p style="color: white">Halaman Login Portal Admin Simpleweb</p>-->
                        </div>
                        <!-- <a href="welcome" class="logo-admin"><img src="<?=base_url()?>assets/login/logo_dark.png" width="20%" alt="logo"></a> -->
                    </div>

                    <div class="p-3">

                    <?php
                                $info = $this->session->flashdata('info');
                                $pesan = $this->session->flashdata('message');
                                
                                 ?>
                                
                               
                                
                                   <?php echo form_open('portal/cek_login', 'class=" m-t-10"');   
                                  
                                  if( $info == 'danger'){ ?>
                                      
                                      <span style="color:red"><?=$pesan?> </span>
                                     
                                  <?php } ?>
                                  
                                
                                  
                                  <?php if( $info == 'success'){ ?>
                                      
                                      <span style="color:green"><?=$pesan?> </span>
                                     
                                  <?php } ?>

                                                
                     

                            <div class="form-group">
                                <label for="email" style="color: white">Username</label>
                                <input type="text" class="form-control"  name="user" placeholder="Enter Username">
                            </div>

                            <div class="form-group">
                                <label for="password" style="color: white">Password</label>
                                <input type="password" class="form-control" placeholder="Enter Password" name="pass">
                            </div>

                          

                            <div class="form-group row m-t-30">
                                <div class="col-sm-6">
                                    <div class="custom-control custom-checkbox">
                                    <input id="satu" style="width:30px; text-align:center"   readonly name="satu" value="<?php echo (rand(1,9)); ?>" > <span style="color:#fff">+</span> 
                                    <input style="width:30px; text-align:center" id="dua"  readonly name="dua" value="<?php echo (rand(1,9)); ?>" > <span style="color:#fff">=</span>
                                    <input style= "width:60px; text-align:center" name="jawaban"  type="number" placeholder="Hasil" required >
                                    </div>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <button class="btn btn-primary w-md waves-effect waves-light" type="submit">Log In</button>
                                </div>
                            </div>

                            

                            <?php echo form_close(); ?>
            <div class="m-t-40 text-center text-white-50">
                <p style="color: white">©<?=date('Y')?> Simpleweb<br/></p>
            </div>

                    </div>

                </div>
            </div>

        </div>


        <!-- jQuery  -->
        <script src="<?=base_url()?>assets/login/jquery.min.js"></script>
        <script src="<?=base_url()?>assets/login/bootstrap.bundle.min.js"></script>
        <script src="<?=base_url()?>assets/login/modernizr.min.js"></script>
        <script src="<?=base_url()?>assets/login/waves.js"></script>
        <script src="<?=base_url()?>assets/login/jquery.slimscroll.js"></script>

        <!-- App js -->
        <script src="<?=base_url()?>assets/login/app.js"></script>

    </body>
</html>