  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Data Histori Log</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-12 col-xs-12">

          <div class="box">
            <div class="box-header">
              <!-- <a href="<?= base_url() ?>keloladata/tambah"><button type="button" class="btn btn-sm btn-primary">Tambah Data</button></a>  -->
              <!--<a onclick="return confirm('Anda Ingin Non Aktifkan Semua User ?')" href="<?= base_url() ?>kelolauser/close_masal"><button type="button" class="btn btn-sm btn-danger">Non Aktifkan Massal</button></a> -->
              <!--<a onclick="return confirm('Anda Ingin Aktifkan Semua User ?')" href="<?= base_url() ?>kelolauser/open_masal"><button type="button" class="btn btn-sm btn-success">Aktifkan Massal</button></a> -->
            </div>

            <!-- /.box-header -->
            <div class="box-body">
              <table style="width:100%" id="example" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th width="50px">Nomor</th>
                    <th>Deskripsi Log</th>
                    <th>Pada Waktu</th>

                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 0;
                  foreach ($alldata as $all) :
                    $no++;

                    $warna = $all->action;
                    switch ($warna) {
                      case 'Menambah':
                        $tampil = '<span style="background-color:green; color:white"> ' . $all->action . '</span> data';
                        break;
                      case 'Mengubah':
                        $tampil = '<span style="background-color:yellow">' . $all->action . '</span> data menjadi';
                        break;
                      case 'Menghapus':
                        $tampil = '<span style="background-color:red; color:white">' . $all->action . '</span> data';
                        break;
                      default:
                        $tampil = '';
                    }
                  ?>

                    <td><?= $no ?></td>
                    <td>Admin <?= $all->username ?> <?= strtolower($tampil) ?> <br /> 
                    <u>
                    ID : <?= $all->id ?><br/>
                    Nama: <?= $all->nama ?><br /> 
                    Alamat: <?= $all->alamat ?><br /> 
                    Umur: <?php $x = date('Y', strtotime($all->tanggal_lahir));
                                                                                                                                                                    echo date('Y') - $x; ?></u></td>
                    <td><?= $this->formatter->getDateTimeMonthFormatUser($all->created_at) ?> </td>
                    </tr>
                  <?php endforeach; ?>

                </tbody>


              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->



        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

  <?php include(__DIR__ . "/../template/footer.php"); ?>




  <script>
    $('#example').DataTable({
      // dom: 'Bfrtip',
      // buttons: [
      //   //'copyHtml5',
      //   'excelHtml5',
      //   //'csvHtml5',
      //   //'pdfHtml5'
      // ],
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "scrollX": true,
      "fixedHeader": true
    });
  </script>