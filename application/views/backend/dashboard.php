 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <?php if($this->session->flashdata('info') != ''){ ?>
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> <?=$this->session->flashdata('notice')?></h4>
                <?=$this->session->flashdata('message')?>
              </div>
      <?php } ?>

 <!-- Small boxes (Stat box) -->
 <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>Menu</h3>

              <!-- <p>New Orders</p> -->
            </div>
            <div class="icon">
              <i class="fa fa-bars"></i>
            </div>
            <a href="<?= base_url() ?>kelolamenu" class="small-box-footer">Menuju menu <i class="fa fa-bars"></i></a>
          </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>Users</h3>

              <!-- <p>New Orders</p> -->
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
            <a href="<?= base_url() ?>kelolauser" class="small-box-footer">Menuju menu user <i class="fa fa-users"></i></a>
          </div>
        </div>
        <!-- ./col -->


        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>Data</h3>

              <!-- <p>New Orders</p> -->
            </div>
            <div class="icon">
              <i class="fa fa-database"></i>
            </div>
            <a href="<?= base_url() ?>keloladata" class="small-box-footer">Menuju menu data <i class="fa fa-database"></i></a>
          </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>Data</h3>

              <!-- <p>New Orders</p> -->
            </div>
            <div class="icon">
              <i class="fa fa-history"></i>
            </div>
            <a href="<?= base_url() ?>kelolahistori" class="small-box-footer">Menuju menu history log <i class="fa fa-history"></i></a>
          </div>
        </div>
        <!-- ./col -->

        <?php
        $menus = $this->db->query("SELECT * FROM si_menu WHERE tipe = 0")->result();
        foreach ($menus as $mn) : ?>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-gray">
            <div class="inner">
              <h3><?=$mn->nama?></h3>

              <!-- <p>New Orders</p> -->
            </div>
            <div class="icon">
              <i class="fa <?=$mn->icon?>"></i>
            </div>
            <a href="<?= base_url('menu/index/'.$mn->url.'/'.$mn->id) ?>" class="small-box-footer">Menuju menu <?=$mn->nama?> <i class="fa <?=$mn->icon?>"></i></a>
          </div>
        </div>
        <!-- ./col -->

         
        <?php endforeach; ?>

        


      </div>
      <!-- /.row -->
      
     
      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include(__DIR__ . "/template/footer.php"); ?>


  