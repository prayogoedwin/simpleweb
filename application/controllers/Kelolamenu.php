<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kelolamenu extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('M_kelolamenu');
    }

    public function index()
	{
        $data['alldata'] = $this->db->query("SELECT * FROM `si_menu` ")->result();
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/menu/index',$data);
	}

    public function tambah()
    {
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/menu/form_tambah');
        
    }

    public function aksi_tambah()
    {
        
            $data = array(
                'nama'           => $this->input->post('nama'),
                'url'         => $this->input->post('url'),
                'icon'  => $this->input->post('icon'),
                'isi'  => $this->input->post('isi'),
            );
            $add = $this->M_kelolamenu->insert($data);
            if($add != FALSE) {
                $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('message', 'Selamat, berhasil');
                redirect(site_url('kelolamenu'));
            }else{
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'Gagal tambah ');
                redirect(site_url('kelolamenu'));
            }
       

    }

    public function edit($id)
    {
        $row = $this->db->query("SELECT * FROM `si_menu` WHERE id ='$id'")->row();
        if ($row) {
                $data['detail']    = $row;
                $this->load->view('backend/template/head');
                $this->load->view('backend/template/header');
                $this->load->view('backend/template/sidebar');
                $this->load->view('backend/menu/form_edit',$data);
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Tidak ditemukan');
            redirect(site_url('kelolamenu'));
        }
    }

    public function aksi_edit()
    {
            
            $id = $this->input->post('id',TRUE);
            $data = array(
                'nama'           => $this->input->post('nama'),
                'url'         => $this->input->post('url'),
                'icon'  => $this->input->post('icon'),
                'isi'  => $this->input->post('isi'),
            );
            
            
            $data = $this->M_kelolamenu->update($id, $data);
            //echo json_encode($data);
            if($data){

                

            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Berhasil Edit ');
            redirect(site_url('kelolamenu'));
            }else{
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'Gagal edit ');
                redirect(site_url('kelolamenu/edit/'.$id));
            }
          
    }

    public function hapus($id)
    {
        $row = $this->M_kelolamenu->soft_del($id);
        // echo json_encode($row);
        if ($row) {

           

            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'selamat, berhasil ');
            redirect(site_url('kelolamenu'));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'gagal hapus ');
            redirect(site_url('kelolamenu'));
        }
    }
}