<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kelolauser extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('M_kelolauser');
    }


    public function index()
	{
        $data['alldata'] = $this->M_kelolauser->get_all_admin();
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/user/admin',$data);
	}



    public function tambah()
    {
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/user/form_user');
        
    }

    public function aksi_tambah()
    {
        
            $data = array(
                'username'          => $this->input->post('username'),
                'password'          => sha1($this->input->post('password')),
                'role'              => $this->input->post('role'),
                'nama'              => $this->input->post('nama'),
            );
            $add = $this->M_kelolauser->insert($data);
            if($add) {
                $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('message', 'selamat, berhasil');
                redirect(site_url('kelolauser/admin'));
            }else{
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'gagal tambah data');
                redirect(site_url('kelolauser/admin'));
            }
       

    }

    public function edit($id)
    {
        $row = $this->M_kelolauser->get_by_id($id);
        if ($row) {
                $data['detail']    = $row;
                $this->load->view('backend/template/head');
                $this->load->view('backend/template/header');
                $this->load->view('backend/template/sidebar');
                $this->load->view('backend/user/form_user_edit',$data);
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'gagal edit');
            redirect(site_url('kelolauser'));
        }
    }

    public function aksi_edit()
    {
            
            $id = $this->input->post('id',TRUE);

            if($this->input->post('password') == ''){
                $data = array(
                    'username'          => $this->input->post('username'),
                    'role'              => $this->input->post('role'),
                    'nama'              => $this->input->post('nama'),
                );
            }else{
                $data = array(
                    'username'          => $this->input->post('username'),
                    'password'          => sha1($this->input->post('password')),
                    'role'              => $this->input->post('role'),
                    'nama'              => $this->input->post('nama'),
                );
            }
            
            
            $data = $this->M_kelolauser->update($id, $data);
            //echo json_encode($data);
            if($data){
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Berhasil Edit ');
            redirect(site_url('kelolauser'));
            }else{
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'gagal edit ');
                redirect(site_url('kelolauser/edit/'.$id));
            }
          
    }

    public function hapus($id)
    {
        $row = $this->M_kelolauser->soft_del($id);
        // echo json_encode($row);
        if ($row) {
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'selamat, berhasil ');
            redirect(site_url('kelolauser'));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'gagal hapus ');
            redirect(site_url('kelolauser'));
        }
    }


    public function hapus_adm($id)
    {
        $row = $this->M_kelolauser->soft_del($id);
        // echo json_encode($row);
        if ($row) {
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'selamat, berhasil ');
            redirect(site_url('kelolauser/admin'));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'gagal hapus ');
            redirect(site_url('kelolauser/admin'));
        }
    }

    public function reset($id)
    {
       
        $row = $this->M_kelolauser->get_by_id($id);
        if ($row) {
                $this->M_kelolauser->reset($id);
                $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('message', 'Reset Password Berhasil');
                redirect(site_url('kelolauser'));
        } else {
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'Reset Password Gagal');
                redirect(site_url('kelolauser'));
        }
    }

    public function reset_adm($id)
    {
       
        $row = $this->M_kelolauser->get_by_id($id);
        if ($row) {
                $this->M_kelolauser->reset($id);
                $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('message', 'Reset Password Berhasil');
                redirect(site_url('kelolauser/admin'));
        } else {
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'Reset Password Gagal');
                redirect(site_url('kelolauser/admin'));
        }
    }

    public function aktifkan($id)
    {
       
        $row = $this->M_kelolauser->get_by_id($id);
        if ($row) {
                $this->M_kelolauser->aktif($id);
                $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('message', 'Aktifkan User Berhasil');
                redirect(site_url('kelolauser'));
        } else {
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'Aktifkan User Gagal');
                redirect(site_url('kelolauser'));
        }
    }

    public function nonaktifkan($id)
    {
       
        $row = $this->M_kelolauser->get_by_id($id);
        if ($row) {
                $this->M_kelolauser->non($id);
                $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('message', 'Non Aktifkan User Berhasil');
                redirect(site_url('kelolauser'));
        } else {
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'Non Aktifkan User Gagal');
                redirect(site_url('kelolauser'));
        }
    }

    public function close_masal()
    {
       
        $row = $this->M_kelolauser->close_all();
        // echo json_encode($row);
        if ($row) {     
                $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('message', 'Non Aktifkan User Berhasil');
                redirect(site_url('kelolauser'));
        } else {
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'Non Aktifkan User Gagal');
                redirect(site_url('kelolauser'));
        }
    }
    public function open_masal()
    {
       
        $row = $this->M_kelolauser->open_all();
        if ($row) {     
                $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('message', 'Aktifkan User Berhasil');
                redirect(site_url('kelolauser'));
        } else {
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'Aktifkan User Gagal');
                redirect(site_url('kelolauser'));
        }
    }



}

/* End of file Banner.php */
/* Location: ./application/controllers/Banner.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-21 07:00:03 */
/* http://harviacode.com */
