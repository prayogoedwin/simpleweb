<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kelolahistori extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('M_keloladata');
    }

    public function index()
	{
        $data['alldata'] = $this->db->query("SELECT a.*, b.username, c.nama, c.alamat, c.tanggal_lahir
        FROM si_log a 
        INNER JOIN si_user b ON b.id_pengguna = a.user
        INNER JOIN si_data c ON c.id = a.data ORDER BY id DESC")->result();
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/histori/index',$data);
	}

    
}