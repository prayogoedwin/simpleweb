<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kelolaisi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('M_kelolaisi');
    }

    public function ajax_cari_id() {
        $id = $this->input->get('search');

        if($id != ''){

            $query = $this->db->query("SELECT * FROM si_data WHERE id = $id");
            if ($query->num_rows() <> 0) {
                $row = $query->row();
                echo '{"results":[{"nip":"'.$row->id.'", 
                     "id":"'.$row->id.'", 
                     "nama":"'.$row->nama.'", 
                     "alamat":"'.$row->alamat.'",
                     "umur":"'.umur($row->tanggal_lahir).'"
                    }]}';
                }
        }else{
            echo '-';
        }
    }
    
    public function search(){
         $id = $this->input->post('id'); //array of id
        // for($x = 0; $x < sizeof($id); $x++){
        //     echo $id[$x];
        // }
        
        echo '<title>Copy Data</title>';

        $set=get_setting(3)->nilai;
        echo '<link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
        <script>window.onload = textarea.select();</script>
        ';
            
            echo "<div class='row container' style='margin-top:40px'>";
            echo "<div class='col-md-10'>";
            echo "<textarea id='textarea' class='form-control'>";

        for($x = 0; $x < sizeof($id); $x++){
            echo $id[$x].$set;
        }
            echo "</textarea>";
            echo '<script>function copy() {
                 let textarea = document.getElementById("textarea");
                 textarea.select();
                 document.execCommand("copy");
                 alert("Berhasil Copy Data!");
                 }
                  $(document).ready(function() {
                    $(\'#summernote\').summernote();
                  });
                 </script>';
            echo '<br><button onclick="copy()">Copy</button>';
            echo "</div>";
            echo "</div>";
            echo '<script>window.onload = textarea.select();</script>';
            
        // echo json_encode($id);

        
        
        
        
    }

    public function index()
	{
        $data['alldata'] = $this->db->query("SELECT a.id as id_isi, a.keterangan, b.* FROM `si_isi` a INNER JOIN `si_data` b ON a.id_data = b.id WHERE b.deleted_at IS NULL ORDER BY b.id DESC")->result();
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/isi/index',$data);
	}

    public function tambah()
    {
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/isi/form_tambah');
        
    }

    public function aksi_tambah()
    {
        
            $data = array(
                'id_user'           => $this->session->userdata('id_pengguna'),
                'id_data'           => $this->input->post('id'),
                'keterangan'         => $this->input->post('ket'),
              
            );
            $add = $this->M_kelolaisi->insert($data);
            if($add != FALSE) {
                // $log = array(
                //         'user'       => $this->session->userdata('id_pengguna'),
                //         'action'     => 'Menambah',
                //         'data'       => $add,
                //         'created_at' => Date('Y-m-d H:i:s'),
                // );
                // $this->M_kelolaisi->insert_log($log);
                $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('message', 'Selamat, berhasil');
                redirect(site_url('kelolaisi'));
            }else{
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'Gagal tambah data');
                redirect(site_url('kelolaisi'));
            }
       

    }

    public function edit($id)
    {
        $row = $this->db->query("SELECT a.id as id_isi, a.keterangan, b.* FROM `si_isi` a INNER JOIN `si_data` b ON a.id_data = b.id WHERE a.id = '$id' AND b.deleted_at IS NULL ORDER BY b.id DESC")->row();
        if ($row) {
                $data['detail']    = $row;
                $this->load->view('backend/template/head');
                $this->load->view('backend/template/header');
                $this->load->view('backend/template/sidebar');
                $this->load->view('backend/isi/form_edit',$data);
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Tidak ditemukan');
            redirect(site_url('kelolaisi'));
        }
    }

    public function aksi_edit()
    {
            
            $id = $this->input->post('id',TRUE);
            $data = array(
                // 'id_user'           => $this->session->userdata('id_pengguna'),
                // 'id_data'           => $this->input->post('id'),
                'keterangan'         => $this->input->post('ket'),
              
            );
            
            
            $data = $this->M_kelolaisi->update($id, $data);
            //echo json_encode($data);
            if($data){

                // $log = array(
                //     'user'       => $this->session->userdata('id_pengguna'),
                //     'action'     => 'Mengubah',
                //     'data'       => $id,
                //     'created_at' => Date('Y-m-d H:i:s'),
                // );
                // $this->M_kelolaisi->insert_log($log);

            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Berhasil Edit ');
            redirect(site_url('kelolaisi'));
            }else{
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'Gagal edit data');
                redirect(site_url('kelolaisi/edit/'.$id));
            }
          
    }

    public function hapus($id)
    {
        $row = $this->M_kelolaisi->delete($id);
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'selamat, berhasil ');
            redirect(site_url('kelolaisi'));
        // // echo json_encode($row);
        // if ($row) {
        //     $this->session->set_flashdata('info', 'success');
        //     $this->session->set_flashdata('message', 'selamat, berhasil ');
        //     redirect(site_url('kelolaisi'));
        // } else {
        //     $this->session->set_flashdata('info', 'danger');
        //     $this->session->set_flashdata('message', 'gagal hapus ');
        //     redirect(site_url('kelolaisi'));
        // }
    }
}