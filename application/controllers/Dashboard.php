<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
    {
        parent::__construct();
        $this->load->model('M_kelolauser');
        $this->load->model('M_dashboard');
        is_login();

	}
	
	public function index()
	{   
        // $data['pelatihan']=$this->db->query("SELECT COUNT(id) as a FROM si_pelatihan WHERE deleted_at IS NULL")->row()->a;
        // $data['peserta']=$this->db->query("SELECT COUNT(id) as a FROM si_pelatihan_pendaftaran WHERE deleted_at IS NULL AND `status` = 2;")->row()->a;
        // $data['user']=$this->db->query("SELECT COUNT(id_pengguna) as a FROM si_user WHERE deleted_at IS NULL")->row()->a;
        $data['pelatihan']=1;
        $data['peserta']=1;
        $data['user']=2;
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/dashboard', $data);
        
	}

	public function edit_password()
    {
		$id = $this->session->userdata('id_pengguna');
        $row = $this->M_kelolauser->get_by_id($id);
        if ($row) {
                $data['detail']    = $row;
                $this->load->view('backend/template/head');
                $this->load->view('backend/template/header');
                $this->load->view('backend/template/sidebar');
                $this->load->view('backend/user/form_edit_password',$data);
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'waduh, gagal ');
            redirect(site_url('dashboard'));
        }
	}

    public function aksi_edit()
    {
            
            $id = $this->input->post('id',TRUE);

            if($this->input->post('password') == ''){
                $data = array(
                    'username'          => $this->input->post('username'),
                    
                    'nama'              => $this->input->post('nama'),
                );
            }else{
                $data = array(
                    'username'          => $this->input->post('username'),
                    'password'          => sha1($this->input->post('password')),
                
                    'nama'              => $this->input->post('nama'),
                );
            }
            
            
            $data = $this->M_kelolauser->update($id, $data);
            //echo json_encode($data);
            if($data){
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Berhasil Edit ');
            redirect(site_url('kelolauser'));
            }else{
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'waduh, gagal ');
                redirect(site_url('kelolauser/edit/'.$id));
            }
          
    }
	
	
	
}
