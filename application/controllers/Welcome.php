<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('backend/login');
	}


	public function login()
	{
		$this->load->helper('url');
		$this->load->view('backend/login');
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(site_url());
	}

	public function cek_login(){
		$user = $this->input->post('user');
		$pass = sha1($this->input->post('pass'));
		$satu = $this->input->post('satu');
		$dua = $this->input->post('dua');
		$jwb = $this->input->post('jawaban');
		$hsl = $satu + $dua;
		
// 				echo $pass;
// 		        echo $user;
		
// 			$login = $this->M_welcome->login($user, $pass);
// 			echo json_encode($login);

		if($hsl == $jwb){
			
			$this->load->model('M_welcome');
			$login = $this->M_welcome->login($user, $pass);
			if($login){
				$this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('notice', 'BERHASIL');
				$this->session->set_flashdata('message', 'Selamat Datang, Anda Berhasil Melakukan Login ke Simpleweb');
				redirect(site_url('dashboard'));
			}else{
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('notice', 'GAGAL');
				$this->session->set_flashdata('message', 'Maaf, login gagal');
				redirect(site_url('portal'));
			}

		}else{
			$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('notice', 'GAGAL');
				$this->session->set_flashdata('message', 'Maaf, captcha salah');
				redirect(site_url('portal'));
		}
	}

	public function cek_login_peserta(){
		$user = $this->input->post('user');
		$pass = sha1($this->input->post('password'));
		
// 		echo $pass;
// 		echo $user;
		
// 			$login = $this->M_welcome->login_peserta($user, $pass);
// 			echo json_encode($login);
		
			
		$this->load->model('M_welcome');
		$login = $this->M_welcome->login_peserta($user, $pass);
		if($login){
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('notice', 'BERHASIL');
			$this->session->set_flashdata('message', 'Selamat Datang, Anda Berhasil Melakukan Login ke Web BLK Semarang 1 Jawa Tengah');
			redirect(site_url('publik/dashboard'));
			// echo "logged in";
		}else{
			$this->session->set_flashdata('info', 'danger');
			$this->session->set_flashdata('notice', 'GAGAL');
			$this->session->set_flashdata('message', 'Maaf, login gagal');
			redirect(site_url('publik/login'));
		}

		
	}

	public function listKecamatan(){
        // Ambil data ID Kota yang dikirim via ajax post
        $id_kota = $this->input->post('id_kota');

        $kecamatan = $this->Auto_model->get_kecamatan_by_kota($id_kota);
        // Buat variabel untuk menampung tag-tag option nya
        // Set defaultnya dengan tag option Pilih
        $lists = "<option value=''>Silakan Pilih Kecamatan</option>";

        foreach($kecamatan as $kec){
          $lists .= "<option value='".$kec->id."'>".$kec->name."</option>"; // Tambahkan tag option ke variabel $lists
        }

        $callback = array('list_kecamatan'=>$lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota
        echo json_encode($callback); // konversi varibael $callback menjadi JSON
	}

	public function listDesa(){
        // Ambil data ID Kecamata yang dikirim via ajax post
        $id_kecamatan = $this->input->post('id_kecamatan');

        $desa = $this->Auto_model->get_desa_by_kecamatan($id_kecamatan);
        // Buat variabel untuk menampung tag-tag option nya
        // Set defaultnya dengan tag option Pilih
        $lists = "<option value=''>Silakan Pilih Desa</option>";

        foreach($desa as $ds){
          $lists .= "<option value='".$ds->id."'>".$ds->name."</option>"; // Tambahkan tag option ke variabel $lists
        }

        $callback = array('list_desa'=>$lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota
        echo json_encode($callback); // konversi varibael $callback menjadi JSON
    }
}
