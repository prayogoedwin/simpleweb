<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('M_setting');
	}
	
	public function index()
	{   
        $data['alldata'] = $this->db->query("SELECT * FROM `si_setting`")->result();
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/setting/index',$data);
	}

    public function aksi_edit()
    {
            
            $id = $this->input->post('id',TRUE);

            
                $data = array(
                    'nilai'          => $this->input->post('nilai')
                );
           
            
            
            $data = $this->M_setting->update($id, $data);
            //echo json_encode($data);
            if($data){
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Berhasil Edit ');
            redirect(site_url('setting'));
            }else{
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'Gagal edit ');
                redirect(site_url('setting'));
            }
          
    }

}
?>