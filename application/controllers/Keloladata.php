<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Keloladata extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('M_keloladata');
    }

    public function index()
	{
        $data['alldata'] = $this->db->query("SELECT * FROM `si_data` WHERE deleted_at IS NULL ORDER BY id DESC")->result();
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/data/index',$data);
	}

    public function form()
	{
        $data['alldata_alamat'] = $this->db->query("SELECT DISTINCT(alamat) AS alamat FROM `si_data` WHERE deleted_at IS NULL ORDER BY id DESC")->result();
        $data['alldata_umur'] = $this->db->query("SELECT DISTINCT(TIMESTAMPDIFF(YEAR, tanggal_lahir, CURDATE())) AS umur FROM `si_data` WHERE deleted_at IS NULL ORDER BY id DESC")->result();
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/data/form_cari',$data);
	}

    public function search_v2(){
       $year = date('Y');
       $umur = $this->input->post('umur');
       $alamat = $this->input->post('alamat');

       if($umur == ''){
           $thn = [''];
       }else{
            for($x = 0; $x < sizeof($umur); $x++){
                $thn[] = $year - $umur[$x];
            }
       }
        
        // echo json_encode($tanggal);
        $set=get_setting(3)->nilai;
        $this->db->select('id');
        $this->db->where_not_in('YEAR(tanggal_lahir)', $thn);
        $this->db->where_not_in('alamat', $alamat);
        $data = $this->db->get('si_data')->result();

        echo '<link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
        <script>window.onload = textarea.select();</script>
        ';
            
            echo "<div class='row container' style='margin-top:40px'>";
            echo "<div class='col-md-10'>";
            echo "<textarea id='textarea' class='form-control'>";

            foreach($data as $d){
                echo $d->id.$set;
            }
            echo "</textarea>";
            echo '<script>function copy() {
                    let textarea = document.getElementById("textarea");
                    textarea.select();
                    document.execCommand("copy");
                    }
                    $(document).ready(function() {
                    $(\'#summernote\').summernote();
                    });
                    </script>';
            echo '<br><button onclick="copy()">Copy</button>';
            echo "</div>";
            echo "</div>";
            echo '<script>window.onload = textarea.select();</script>';

        // echo json_encode($data);


           
    //    echo json_encode($umur);
       
   }
	
	public function search(){
        $id = $this->input->post('id'); //array of id
       // for($x = 0; $x < sizeof($id); $x++){
       //     echo $id[$x];
       // }

       $set=get_setting(3)->nilai;
       echo '<link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
       <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
       <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
       <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
       <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
       <script>window.onload = textarea.select();</script>
       ';
           
           echo "<div class='row container' style='margin-top:40px'>";
           echo "<div class='col-md-10'>";
           echo "<textarea id='textarea' class='form-control'>";

       for($x = 0; $x < sizeof($id); $x++){
           echo $id[$x].$set;
       }
           echo "</textarea>";
           echo '<script>function copy() {
                let textarea = document.getElementById("textarea");
                textarea.select();
                document.execCommand("copy");
                }
                 $(document).ready(function() {
                   $(\'#summernote\').summernote();
                 });
                </script>';
           echo '<br><button onclick="copy()">Copy</button>';
           echo "</div>";
           echo "</div>";
           echo '<script>window.onload = textarea.select();</script>';
           
       // echo json_encode($id);
       
   }

    public function tambah()
    {
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/data/form_tambah');
        
    }

    public function aksi_tambah()
    {
        
            $data = array(
                'nama'           => $this->input->post('nama'),
                'alamat'         => $this->input->post('alamat'),
                'tanggal_lahir'  => $this->input->post('tanggal_lahir'),
            );
            $add = $this->M_keloladata->insert($data);
            if($add != FALSE) {
                $log = array(
                        'user'       => $this->session->userdata('id_pengguna'),
                        'action'     => 'Menambah',
                        'data'       => $add,
                        'created_at' => Date('Y-m-d H:i:s'),
                );
                $this->M_keloladata->insert_log($log);
                $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('message', 'Selamat, berhasil');
                redirect(site_url('keloladata'));
            }else{
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'Gagal tambah data');
                redirect(site_url('keloladata'));
            }
       

    }

    public function edit($id)
    {
        $row = $this->db->query("SELECT * FROM `si_data` WHERE id ='$id'")->row();
        if ($row) {
                $data['detail']    = $row;
                $this->load->view('backend/template/head');
                $this->load->view('backend/template/header');
                $this->load->view('backend/template/sidebar');
                $this->load->view('backend/data/form_edit',$data);
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Tidak ditemukan');
            redirect(site_url('keloladata'));
        }
    }

    public function aksi_edit()
    {
            
            $id = $this->input->post('id',TRUE);
            $data = array(
                'nama'           => $this->input->post('nama'),
                'alamat'         => $this->input->post('alamat'),
                'tanggal_lahir'  => $this->input->post('tanggal_lahir'),
               
            );
            
            
            $data = $this->M_keloladata->update($id, $data);
            //echo json_encode($data);
            if($data){

                $log = array(
                    'user'       => $this->session->userdata('id_pengguna'),
                    'action'     => 'Mengubah',
                    'data'       => $id,
                    'created_at' => Date('Y-m-d H:i:s'),
                );
                $this->M_keloladata->insert_log($log);

            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Berhasil Edit ');
            redirect(site_url('keloladata'));
            }else{
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'Gagal edit data');
                redirect(site_url('keloladata/edit/'.$id));
            }
          
    }

    public function hapus($id)
    {
        $row = $this->M_keloladata->soft_del($id);
        // echo json_encode($row);
        if ($row) {

            $log = array(
                'user'       => $this->session->userdata('id_pengguna'),
                'action'     => 'Menghapus',
                'data'       => $id,
                'created_at' => Date('Y-m-d H:i:s'),
            );
            $this->M_keloladata->insert_log($log);

            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'selamat, berhasil ');
            redirect(site_url('keloladata'));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'gagal hapus ');
            redirect(site_url('keloladata'));
        }
    }
}